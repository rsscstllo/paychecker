//
//  Date.swift
//  Paychecker
//
//  Created by Ross Castillo on 12/1/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import Foundation

class Date {
  class func toString(date: NSDate) -> String {
    let dateStringFormatter = NSDateFormatter()
    dateStringFormatter.dateFormat = "MM/dd/yy"
    let dateString = dateStringFormatter.stringFromDate(date)
    return dateString
  }
  class func timeToString(date: NSDate) -> String {
    let dateStringFormatter = NSDateFormatter()
    dateStringFormatter.dateFormat = "hh:mm a"
    let dateString = dateStringFormatter.stringFromDate(date)
    return dateString
  }
}
