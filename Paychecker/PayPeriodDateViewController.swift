//
//  PayPeriodDateViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/30/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

class PayPeriodDateViewController: UIViewController {
  
  var date: NSDate!
  var indexPath: Int!
  var payPeriodVC: PayPeriodViewController!
  var shiftVC: ShiftViewController!
  
  @IBOutlet weak var datePicker: UIDatePicker!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    if date != nil {
      datePicker.date = date
    }
    
    // TODO: Change this
    if title == "Start Date" || title == "End Date" || title == "Shift Date" {
      datePicker.datePickerMode = UIDatePickerMode.Date
      
    } else if title == "Clock-In" || title == "Clock-Out" {
      datePicker.datePickerMode = UIDatePickerMode.Time
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBActions
  
  @IBAction func cancelBarButtonItemTapped(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func saveBarButtonItemTapped(sender: UIBarButtonItem) {
    
    if payPeriodVC != nil {
      if indexPath == 0 {
        payPeriodVC.startDate = datePicker.date
      } else if indexPath == 1 {
        payPeriodVC.endDate = datePicker.date
      }
    } else if shiftVC != nil {
      if indexPath == 0 {
        shiftVC.shiftDate = datePicker.date
      } else if indexPath == 1 {
        shiftVC.clockInTime = datePicker.date
      } else if indexPath == 2 {
        shiftVC.clockOutTime = datePicker.date
      }
    }
    
    dismissViewControllerAnimated(true, completion: nil)
  }
  
}
