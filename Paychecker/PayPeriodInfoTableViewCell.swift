//
//  PayPeriodInfoTableViewCell.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/30/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

class PayPeriodInfoTableViewCell: UITableViewCell {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
    detailTextLabel?.text = ""
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
}
