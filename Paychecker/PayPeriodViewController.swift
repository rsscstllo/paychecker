//
//  PayPeriodViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/21/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Parse

class PayPeriodViewController: UIViewController {
  
  var payPeriod: PFObject!
  @IBOutlet weak var tableView: UITableView!
  
  // temp vars
  var startDate: NSDate!
  var endDate: NSDate!
  var federalIncomeTax: Double!
  var socialSecurityTax: Double!
  var medicareTax: Double!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    if payPeriod != nil {
      startDate = payPeriod.objectForKey("startDate") as! NSDate
      endDate = payPeriod.objectForKey("endDate") as! NSDate
      federalIncomeTax = payPeriod.objectForKey("federalIncomeTax") as! Double
      socialSecurityTax = payPeriod.objectForKey("socialSecurityTax") as! Double
      medicareTax = payPeriod.objectForKey("medicareTax") as! Double
    }
    
    tableView.dataSource = self
    tableView.delegate = self
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    tableView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBAcitons
  
  @IBAction func cancelBarButtonItemTapped(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func saveBarButtonItemTapped(sender: UIBarButtonItem) {
    if startDate == nil || endDate == nil || federalIncomeTax == nil || socialSecurityTax == nil || medicareTax == nil {
      let alert = UIAlertController(title: "Could Not Save", message: "Please enter all fields.", preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
      presentViewController(alert, animated: true, completion: nil)
      
    } else {
      
      if payPeriod == nil {
        
        let payPeriod: PFObject = PFObject(className: "PayPeriod")
        payPeriod["userID"] = PFUser.currentUser()?.objectId
        payPeriod["startDate"] = startDate
        payPeriod["endDate"] = endDate
        payPeriod["federalIncomeTax"] = federalIncomeTax
        payPeriod["socialSecurityTax"] = socialSecurityTax
        payPeriod["medicareTax"] = medicareTax
        payPeriod["totalHours"] = 0.0
        payPeriod["totalPay"] = 0.0
        payPeriod["totalTax"] = 0.0
        payPeriod["totalNetPay"] = 0.0
        
        payPeriod.saveInBackgroundWithBlock {
          (success: Bool, error: NSError?) -> Void in
          if (success) {
            print("Pay period has been uploaded and saved to parse")
            
            self.dismissViewControllerAnimated(true, completion: nil)
          } else {
            print(error?.description)
          }
        }
        
      } else if payPeriod != nil {
        
        payPeriod["startDate"] = startDate
        payPeriod["endDate"] = endDate
        payPeriod["federalIncomeTax"] = federalIncomeTax
        payPeriod["socialSecurityTax"] = socialSecurityTax
        payPeriod["medicareTax"] = medicareTax
        payPeriod.saveInBackgroundWithBlock {
          (success: Bool, error: NSError?) -> Void in
          if (success) {
            print("Pay period has been uploaded and saved to parse")
            self.dismissViewControllerAnimated(true, completion: nil)
          } else {
            print(error?.description)
          }
        }
      }
      
    } // end of else
  }
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let nav = segue.destinationViewController as! UINavigationController
    if let indexPath = tableView.indexPathForSelectedRow {
      if segue.identifier == "modalStartDate" {
        let dateVC: PayPeriodDateViewController = nav.topViewController as! PayPeriodDateViewController
        dateVC.title = "Start Date"
        dateVC.indexPath = indexPath.row
        dateVC.payPeriodVC = self
        if startDate != nil {
          dateVC.date = startDate
        }
        
      } else if segue.identifier == "modalEndDate" {
        let dateVC: PayPeriodDateViewController = nav.topViewController as! PayPeriodDateViewController
        dateVC.title = "End Date"
        dateVC.indexPath = indexPath.row
        dateVC.payPeriodVC = self
        if endDate != nil {
          dateVC.date = endDate
        }
        
      } else if segue.identifier == "modalFederalTax" {
        let federalVC: TaxViewController = nav.topViewController as! TaxViewController
        federalVC.title = "Federal Income Tax"
        federalVC.indexPath = indexPath.row
        federalVC.payPeriodVC = self
        if federalIncomeTax != nil {
          federalVC.text = "\(federalIncomeTax)"
        }
        
      } else if segue.identifier == "modalSSTax" {
        let ssVC: TaxViewController = nav.topViewController as! TaxViewController
        ssVC.title = "Social Security Tax"
        ssVC.indexPath = indexPath.row
        ssVC.payPeriodVC = self
        if socialSecurityTax != nil {
          ssVC.text = "\(socialSecurityTax)"
        }
        
      } else if segue.identifier == "modalMedicareTax" {
        let medicareVC: TaxViewController = nav.topViewController as! TaxViewController
        medicareVC.title = "Medicare Tax"
        medicareVC.indexPath = indexPath.row
        medicareVC.payPeriodVC = self
        if medicareTax != nil {
          medicareVC.text = "\(medicareTax)"
        }
      }
    }
  }
  
}

// MARK: - UITableViewDataSource

extension PayPeriodViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: PayPeriodInfoTableViewCell = tableView.dequeueReusableCellWithIdentifier("payPeriodInfoCell") as! PayPeriodInfoTableViewCell
    if indexPath.row == 0 {
      cell.textLabel?.text = "Start Date:"
      if startDate != nil {
        cell.detailTextLabel?.text = Date.toString(startDate)
      }
      return cell
      
    } else if indexPath.row == 1 {
      cell.textLabel?.text = "End Date:"
      if endDate != nil {
        cell.detailTextLabel?.text = Date.toString(endDate)
      }
      return cell
      
    } else if indexPath.row == 2 {
      cell.textLabel?.text = "Federal Income Tax:"
      if federalIncomeTax != nil {
        cell.detailTextLabel?.text = "\(federalIncomeTax)%"
      }
      return cell
      
    } else if indexPath.row == 3 {
      cell.textLabel?.text = "Social Security Tax:"
      if socialSecurityTax != nil {
        cell.detailTextLabel?.text = "\(socialSecurityTax)%"
      }
      return cell
      
    } else if indexPath.row == 4 {
      cell.textLabel?.text = "Medicare Tax:"
      if medicareTax != nil {
        cell.detailTextLabel?.text = "\(medicareTax)%"
      }
      return cell
      
    } else {
      return cell
    }
  }
}

// MARK: - UITableViewDelegate

extension PayPeriodViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row == 0 {
      performSegueWithIdentifier("modalStartDate", sender: self)
      
    } else if indexPath.row == 1 {
      performSegueWithIdentifier("modalEndDate", sender: self)
      
    } else if indexPath.row == 2 {
      performSegueWithIdentifier("modalFederalTax", sender: self)
      
    } else if indexPath.row == 3 {
      performSegueWithIdentifier("modalSSTax", sender: self)
      
    } else if indexPath.row == 4 {
      performSegueWithIdentifier("modalMedicareTax", sender: self)
      
    } else {
    }
  }
}
