//
//  PayPeriodsViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/16/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class PayPeriodsViewController: UIViewController {
  
  var payPeriods: [PFObject] = []
  @IBOutlet weak var payPeriodsTableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    payPeriodsTableView.dataSource = self
    payPeriodsTableView.delegate = self
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    
    if (PFUser.currentUser() == nil) {
      presentLogin();
      
    } else {
      
      payPeriods = []
      // Query for current user's pay periods
      let query = PFQuery(className: "PayPeriod")
      query.orderByDescending("createdAt")
      query.whereKey("userID", equalTo: (PFUser.currentUser()?.objectId)!)
      query.findObjectsInBackgroundWithBlock {
        (objects: [PFObject]?, error: NSError?) -> Void in
        if error == nil {
          // The find succeeded
          print("Successfully retrieved \(objects!.count) pay periods.")
          // Do something with the found objects
          if let objects = objects {
            for object in objects {
              // print(object.objectId)
              self.payPeriods.append(object)
            } // End for loop
          }
          self.payPeriodsTableView.reloadData()
        } else {
          // Log details of the failure
          print("Error: \(error!) \(error!.userInfo)")
        }
      } // End of block
      
      payPeriodsTableView.reloadData()
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func asdf(dec: Double) -> String {
    let str = String(format: "%g", dec)
    return str
  }
  
  // MARK: - IBAcitons
  
  @IBAction func settingsBarButtonItemTapped(sender: UIBarButtonItem) {
    performSegueWithIdentifier("showSettings", sender: self)
  }
  
  @IBAction func plusBarButtonItemTapped(sender: UIBarButtonItem) {
    performSegueWithIdentifier("modalAddPayPeriod", sender: self)
  }
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "showShifts" {
      if let indexPath = payPeriodsTableView.indexPathForSelectedRow {
        let thisPayPeriod: PFObject = payPeriods[indexPath.row]
        let shiftsVC: ShiftsViewController = segue.destinationViewController as! ShiftsViewController
        shiftsVC.payPeriod = thisPayPeriod
      }
    } else if segue.identifier == "modalAddPayPeriod" {
      let nav = segue.destinationViewController as! UINavigationController
      let addPayPeriodVC: PayPeriodViewController = nav.topViewController as! PayPeriodViewController
      addPayPeriodVC.title = "Add Pay Period"
    }
  }
  
  // MARK: - Helper
  
  private func presentLogin() {
    let logInViewController: PFLogInViewController = PFLogInViewController()
    let signUpViewController: PFSignUpViewController = PFSignUpViewController()
    logInViewController.fields = [PFLogInFields.UsernameAndPassword, PFLogInFields.LogInButton, PFLogInFields.SignUpButton, PFLogInFields.PasswordForgotten]
    let logInLogoTitle = UILabel()
    logInLogoTitle.text = "Paychecker"
    logInLogoTitle.font = UIFont(name: "American Typewriter", size: 40)
    logInViewController.logInView?.logo = logInLogoTitle
    let signUpLogoTitle = UILabel()
    signUpLogoTitle.text = "Paychecker"
    signUpLogoTitle.font = UIFont(name: "American Typewriter", size: 40)
    signUpViewController.signUpView?.logo = signUpLogoTitle
    logInViewController.delegate = self
    signUpViewController.delegate = self
    logInViewController.signUpController = signUpViewController
    presentViewController(logInViewController, animated: true, completion: nil)
  }
  
}

// MARK: - PFLogInViewControllerDelegate

extension PayPeriodsViewController: PFLogInViewControllerDelegate {
  func logInViewController(logInController: PFLogInViewController, shouldBeginLogInWithUsername username: String, password: String) -> Bool {
    if (!username.isEmpty && !password.isEmpty) {
      return true
    } else {
      return false
    }
  }
  func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  func logInViewController(logInController: PFLogInViewController, didFailToLogInWithError error: NSError?) {
    print("Failed to login...")
  }
}

// MARK: - PFSignUpViewControllerDelegate

extension PayPeriodsViewController: PFSignUpViewControllerDelegate {
  func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  func signUpViewController(signUpController: PFSignUpViewController, didFailToSignUpWithError error: NSError?) {
    print("Failed to sign up...")
  }
  func signUpViewControllerDidCancelSignUp(signUpController: PFSignUpViewController) {
    print("User dismissed sign up.")
  }
}

// MARK: - UITableViewDataSource

extension PayPeriodsViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return payPeriods.count
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: PayPeriodTableViewCell = payPeriodsTableView.dequeueReusableCellWithIdentifier("payPeriodCell") as! PayPeriodTableViewCell
    let thisPayPeriod: PFObject = payPeriods[indexPath.row]
    
    cell.dateLabel.text = "\(Date.toString(thisPayPeriod.objectForKey("startDate") as! NSDate)) -> \(Date.toString(thisPayPeriod.objectForKey("endDate") as! NSDate))"
    cell.hoursLabel.text = "\(asdf(thisPayPeriod.objectForKey("totalHours") as! Double)) hrs"
    cell.payLabel.text = "$\(asdf(thisPayPeriod.objectForKey("totalPay") as! Double))"
    cell.taxLabel.text = "-$\(asdf(thisPayPeriod.objectForKey("totalTax") as! Double))"
    cell.netPayLabel.text = "$\(asdf(thisPayPeriod.objectForKey("totalNetPay") as! Double))"
    
    return cell
  }
}

// MARK: - UITableViewDelegate

extension PayPeriodsViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegueWithIdentifier("showShifts", sender: self)
  }
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    let thisPayPeriod: PFObject = payPeriods[indexPath.row]
    payPeriods.removeAtIndex(indexPath.row)
    
    if editingStyle == .Delete {
      
      let query1 = PFQuery(className: "Shift")
      query1.whereKey("payPeriodID", equalTo: thisPayPeriod.objectId!)
      query1.findObjectsInBackgroundWithBlock {
        (objects: [PFObject]?, error: NSError?) -> Void in
        if error == nil {
          // The find succeeded
          print("Successfully retrieved \(objects!.count) shifts to delete.")
          // Do something with the found objects
          if let objects = objects {
            for object in objects {
              // print(object.objectId)
              object.deleteEventually()
            } // End for loop
          }
        } else {
          // Log details of the failure
          print("Error: \(error!) \(error!.userInfo)")
        }
      } // End of block
      
      let query2 = PFQuery(className: "PayPeriod")
      query2.whereKey("objectId", equalTo: thisPayPeriod.objectId!)
      query2.findObjectsInBackgroundWithBlock {
        (objects: [PFObject]?, error: NSError?) -> Void in
        if error == nil {
          // The find succeeded
          print("Successfully retrieved \(objects!.count) pay period to delete.")
          // Do something with the found objects
          if let objects = objects {
            for object in objects {
              // print(object.objectId)
              object.deleteEventually()
            } // End for loop
          }
        } else {
          // Log details of the failure
          print("Error: \(error!) \(error!.userInfo)")
        }
      } // End of block
    }
    payPeriodsTableView.reloadData()
  }
}
