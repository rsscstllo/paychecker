//
//  SettingsViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/21/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Parse

class SettingsViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    tableView.dataSource = self
    tableView.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}

// MARK: - UITableViewDataSource

extension SettingsViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: LogOutTableViewCell = tableView.dequeueReusableCellWithIdentifier("logOutCell") as! LogOutTableViewCell
    cell.textLabel?.text = "Log Out"
    return cell
  }
}

// MARK: - UITableViewDelegate

extension SettingsViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let alert = UIAlertController(title: "Are you sure?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "Log Out", style: UIAlertActionStyle.Default, handler: {
      action in
      PFUser.logOut()
      self.navigationController?.popViewControllerAnimated(true)
    }))
    presentViewController(alert, animated: true, completion: nil)
  }
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "Account Actions"
  }
  func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    return "Paychecker v1.0.0 💵\nMade in Gainesville"
  }
}
