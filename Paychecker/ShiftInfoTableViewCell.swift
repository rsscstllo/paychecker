//
//  ShiftInfoTableViewCell.swift
//  Paychecker
//
//  Created by Ross Castillo on 12/1/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

class ShiftInfoTableViewCell: UITableViewCell {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
    detailTextLabel?.text = ""
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
}
