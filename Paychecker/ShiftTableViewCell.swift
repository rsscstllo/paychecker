//
//  ShiftTableViewCell.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/21/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

class ShiftTableViewCell: UITableViewCell {
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var hoursLabel: UILabel!
  @IBOutlet weak var payLabel: UILabel!
  @IBOutlet weak var taxLabel: UILabel!
  @IBOutlet weak var netPayLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
    netPayLabel.textColor = UIColor(red: 33/255.0, green: 108/255.0, blue: 42/255.0, alpha: 1.0)
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
}
