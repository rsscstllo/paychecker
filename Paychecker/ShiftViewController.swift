//
//  ShiftViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/21/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Parse

class ShiftViewController: UIViewController {
  
  var payPeriod: PFObject!
  var shift: PFObject!
  @IBOutlet weak var tableView: UITableView!
  
  // temp vars
  var shiftDate: NSDate!
  var clockInTime: NSDate!
  var clockOutTime: NSDate!
  var hourlyWage: Double!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    tableView.dataSource = self
    tableView.delegate = self
    
    if shift != nil {
      shiftDate = shift.objectForKey("shiftDate") as! NSDate
      clockInTime = shift.objectForKey("clockInTime") as! NSDate
      clockOutTime = shift.objectForKey("clockOutTime") as! NSDate
      hourlyWage = shift.objectForKey("hourlyWage") as! Double
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    tableView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBActions
  
  @IBAction func cancelBarButtonItemTapped(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func saveBarButtonItemTapped(sender: UIBarButtonItem) {
    if shiftDate == nil || clockInTime == nil || clockOutTime == nil || hourlyWage == nil || payPeriod == nil {
      let alert = UIAlertController(title: "Could Not Save", message: "Please enter all fields.", preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
      presentViewController(alert, animated: true, completion: nil)
      
    } else {
      
      if shift == nil {
        
        let shift: PFObject = PFObject(className: "Shift")
        shift["userID"] = PFUser.currentUser()?.objectId
        shift["payPeriodID"] = payPeriod.objectId
        shift["shiftDate"] = shiftDate
        shift["clockInTime"] = clockInTime
        shift["clockOutTime"] = clockOutTime
        shift["hourlyWage"] = hourlyWage
        
        // 3600 seconds in one hour
        var hours: Double = (clockOutTime.timeIntervalSinceDate(clockInTime)) / 3600
        hours = Double(round(100 * hours) / 100)
        shift["hours"] = hours
        
        var pay: Double = hours * hourlyWage
        pay = Double(round(100 * pay) / 100)
        shift["pay"] = pay
        
        var totalTax: Double = 0.0
        var federalIncomeTax = payPeriod.objectForKey("federalIncomeTax") as! Double
        var socialSecurityTax = payPeriod.objectForKey("socialSecurityTax") as! Double
        var medicareTax = payPeriod.objectForKey("medicareTax") as! Double
        
        federalIncomeTax = federalIncomeTax / 100
        socialSecurityTax = socialSecurityTax / 100
        medicareTax = medicareTax / 100
        
        federalIncomeTax = pay * federalIncomeTax
        socialSecurityTax = pay * socialSecurityTax
        medicareTax = pay * medicareTax
        
        totalTax = federalIncomeTax + socialSecurityTax + medicareTax
        totalTax = Double(round(100 * totalTax) / 100)
        shift["tax"] = totalTax
        
        var netPay = pay - totalTax
        netPay = Double(round(100 * netPay) / 100)
        shift["netPay"] = netPay
        
        shift.saveInBackgroundWithBlock {
          (success: Bool, error: NSError?) -> Void in
          if (success) {
            print("Shift has been uploaded and saved to parse")
            self.dismissViewControllerAnimated(true, completion: nil)
          } else {
            print(error?.description)
          }
        }
        
        
      } else if shift != nil {
        
        // TODO: reduce and abstract calculation code.
        // Just temporarily copied code from above because was short on time.
        
        shift["shiftDate"] = shiftDate
        shift["clockInTime"] = clockInTime
        shift["clockOutTime"] = clockOutTime
        shift["hourlyWage"] = hourlyWage
        
        var hours: Double = (clockOutTime.timeIntervalSinceDate(clockInTime)) / 3600
        hours = Double(round(100 * hours) / 100)
        shift["hours"] = hours
        
        var pay: Double = hours * hourlyWage
        pay = Double(round(100 * pay) / 100)
        shift["pay"] = pay
        
        var totalTax: Double = 0.0
        var federalIncomeTax = payPeriod.objectForKey("federalIncomeTax") as! Double
        var socialSecurityTax = payPeriod.objectForKey("socialSecurityTax") as! Double
        var medicareTax = payPeriod.objectForKey("medicareTax") as! Double
        
        federalIncomeTax = federalIncomeTax / 100
        socialSecurityTax = socialSecurityTax / 100
        medicareTax = medicareTax / 100
        
        federalIncomeTax = pay * federalIncomeTax
        socialSecurityTax = pay * socialSecurityTax
        medicareTax = pay * medicareTax
        
        totalTax = federalIncomeTax + socialSecurityTax + medicareTax
        totalTax = Double(round(100 * totalTax) / 100)
        shift["tax"] = totalTax
        
        var netPay = pay - totalTax
        netPay = Double(round(100 * netPay) / 100)
        shift["netPay"] = netPay
        
        shift.saveInBackgroundWithBlock {
          (success: Bool, error: NSError?) -> Void in
          if (success) {
            print("Shift has been uploaded and saved to parse")
            self.dismissViewControllerAnimated(true, completion: nil)
          } else {
            print(error?.description)
          }
        }
      }
      
    } // End of else
  }
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let nav = segue.destinationViewController as! UINavigationController
    if let indexPath = tableView.indexPathForSelectedRow {
      if segue.identifier == "modalDate" {
        let dateVC: PayPeriodDateViewController = nav.topViewController as! PayPeriodDateViewController
        dateVC.title = "Shift Date"
        dateVC.indexPath = indexPath.row
        dateVC.shiftVC = self
        if shiftDate != nil {
          dateVC.date = shiftDate
        }
        
      } else if segue.identifier == "modalClockInTime" {
        let timeVC: PayPeriodDateViewController = nav.topViewController as! PayPeriodDateViewController
        timeVC.title = "Clock-In"
        timeVC.indexPath = indexPath.row
        timeVC.shiftVC = self
        if clockInTime != nil {
          timeVC.date = clockInTime
        }
        
      } else if segue.identifier == "modalClockOutTime" {
        let timeVC: PayPeriodDateViewController = nav.topViewController as! PayPeriodDateViewController
        timeVC.title = "Clock-Out"
        timeVC.indexPath = indexPath.row
        timeVC.shiftVC = self
        if clockOutTime != nil {
          timeVC.date = clockOutTime
        }
        
      } else if segue.identifier == "modalHourlyWage" {
        let wageVC: TaxViewController = nav.topViewController as! TaxViewController
        wageVC.title = "Hourly Wage"
        wageVC.indexPath = indexPath.row
        wageVC.shiftVC = self
        if hourlyWage != nil {
          wageVC.text = "\(hourlyWage)"
        }
      }
    }
  }
  
}

// MARK: - UITableViewDataSource

extension ShiftViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 4
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: ShiftInfoTableViewCell = tableView.dequeueReusableCellWithIdentifier("shiftInfoCell") as! ShiftInfoTableViewCell
    if indexPath.row == 0 {
      cell.textLabel?.text = "Date:"
      if shiftDate != nil {
        cell.detailTextLabel?.text = Date.toString(shiftDate)
      }
      return cell
      
    } else if indexPath.row == 1 {
      cell.textLabel?.text = "Clock-In Time:"
      if clockInTime != nil {
        cell.detailTextLabel?.text = Date.timeToString(clockInTime)
      }
      return cell
      
    } else if indexPath.row == 2 {
      cell.textLabel?.text = "Clock-Out Time:"
      if clockOutTime != nil {
        cell.detailTextLabel?.text = Date.timeToString(clockOutTime)
      }
      return cell
      
    } else if indexPath.row == 3 {
      cell.textLabel?.text = "Hourly Wage:"
      if hourlyWage != nil {
        cell.detailTextLabel?.text = "$\(hourlyWage)"
      }
      return cell
      
    } else {
      return cell
    }
  }
}

// MARK: - UITableViewDelegate

extension ShiftViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row == 0 {
      performSegueWithIdentifier("modalDate", sender: self)
      
    } else if indexPath.row == 1 {
      performSegueWithIdentifier("modalClockInTime", sender: self)
      
    } else if indexPath.row == 2 {
      performSegueWithIdentifier("modalClockOutTime", sender: self)
      
    } else if indexPath.row == 3 {
      performSegueWithIdentifier("modalHourlyWage", sender: self)
    }
  }
}
