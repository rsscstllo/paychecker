//
//  ShiftsViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/21/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit
import Parse

class ShiftsViewController: UIViewController {
  
  var payPeriod: PFObject!
  var shifts: [PFObject] = []
  @IBOutlet weak var shiftsTableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    shiftsTableView.dataSource = self
    shiftsTableView.delegate = self
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    
    shifts = []
    // Query for current user's shifts
    let predicate = NSCompoundPredicate(type: .AndPredicateType, subpredicates: [NSPredicate(format: "userID = %@", (PFUser.currentUser()?.objectId)!), NSPredicate(format: "payPeriodID = %@", payPeriod.objectId!)])
    let query = PFQuery(className: "Shift", predicate: predicate)
    query.orderByDescending("createdAt")
    query.findObjectsInBackgroundWithBlock {
      (objects: [PFObject]?, error: NSError?) -> Void in
      if error == nil {
        // The find succeeded
        print("Successfully retrieved \(objects!.count) shifts.")
        // Do something with the found objects
        if let objects = objects {
          for object in objects {
            // print(object.objectId)
            self.shifts.append(object)
          } // End for loop
          
          var totalHours = 0.0
          var totalPay = 0.0
          var totalTax = 0.0
          var netPay = 0.0
          
          // iterate through each shift and add totals
          for var i = 0; i < self.shifts.count; i++ {
            totalHours = totalHours + (self.shifts[i].objectForKey("hours") as! Double)
            totalPay = totalPay + (self.shifts[i].objectForKey("pay") as! Double)
            totalTax = totalTax + (self.shifts[i].objectForKey("tax") as! Double)
            netPay = netPay + (self.shifts[i].objectForKey("netPay") as! Double)
          }
          
          totalHours = Double(round(100 * totalHours) / 100)
          totalPay = Double(round(100 * totalPay) / 100)
          totalTax = Double(round(100 * totalTax) / 100)
          netPay = Double(round(100 * netPay) / 100)
          
          self.payPeriod.setValue(totalHours, forKey: "totalHours")
          self.payPeriod.setValue(totalPay, forKey: "totalPay")
          self.payPeriod.setValue(totalTax, forKey: "totalTax")
          self.payPeriod.setValue(netPay, forKey: "totalNetPay")
          self.payPeriod.saveInBackground()
          
          self.shiftsTableView.reloadData()
        }
      } else {
        // Log details of the failure
        print("Error: \(error!) \(error!.userInfo)")
      }
    } // End of block
    shiftsTableView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func asdf(dec: Double) -> String {
    let str = String(format: "%g", dec)
    return str
  }
  
  // MARK: - IBActions
  
  @IBAction func editBarButtonItemTapped(sender: UIBarButtonItem) {
    performSegueWithIdentifier("modalEditPayPeriod", sender: self)
  }
  
  @IBAction func plusBarButtonItemTapped(sender: UIBarButtonItem) {
    performSegueWithIdentifier("modalAddShift", sender: self)
  }
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let nav = segue.destinationViewController as! UINavigationController
    if segue.identifier == "modalAddShift" {
      let addShiftVC: ShiftViewController = nav.topViewController as! ShiftViewController
      addShiftVC.payPeriod = payPeriod
      addShiftVC.title = "Add Shift"
      
    } else if segue.identifier == "modalEditShift" {
      if let indexPath = shiftsTableView.indexPathForSelectedRow {
        let thisShift: PFObject = shifts[indexPath.row]
        let shiftVC: ShiftViewController = nav.topViewController as! ShiftViewController
        shiftVC.title = "Edit Shift"
        shiftVC.shift = thisShift
        shiftVC.payPeriod = payPeriod
      }
      
    } else if segue.identifier == "modalEditPayPeriod" {
      let editPayPeriodVC: PayPeriodViewController = nav.topViewController as! PayPeriodViewController
      editPayPeriodVC.title = "Edit Pay Period"
      editPayPeriodVC.payPeriod = payPeriod
    }
  }
  
}

// MARK: - UITableViewDataSource

extension ShiftsViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return shifts.count
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: ShiftTableViewCell = shiftsTableView.dequeueReusableCellWithIdentifier("shiftCell") as! ShiftTableViewCell
    let thisShift: PFObject = shifts[indexPath.row]
    
    cell.dateLabel.text = "\(Date.toString(thisShift.objectForKey("shiftDate") as! NSDate))"
    cell.hoursLabel.text = "\(asdf(thisShift.objectForKey("hours") as! Double)) hrs"
    cell.payLabel.text = "$\(asdf(thisShift.objectForKey("pay") as! Double))"
    cell.taxLabel.text = "-$\(asdf(thisShift.objectForKey("tax") as! Double))"
    cell.netPayLabel.text = "$\(asdf(thisShift.objectForKey("netPay") as! Double))"
    
    return cell
  }
}

// MARK: - UITableViewDelegate

extension ShiftsViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegueWithIdentifier("modalEditShift", sender: self)
  }
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
      let thisShift: PFObject = shifts[indexPath.row]
      shifts.removeAtIndex(indexPath.row)
      let query = PFQuery(className: "Shift")
      query.whereKey("objectId", equalTo: thisShift.objectId!)
      query.findObjectsInBackgroundWithBlock {
        (objects: [PFObject]?, error: NSError?) -> Void in
        if error == nil {
          // The find succeeded
          print("Successfully retrieved \(objects!.count) shift to delete.")
          // Do something with the found objects
          if let objects = objects {
            for object in objects {
              // print(object.objectId)
              object.deleteEventually()
            } // End for loop
          }
        } else {
          // Log details of the failure
          print("Error: \(error!) \(error!.userInfo)")
        }
      } // End of block
    }
    shiftsTableView.reloadData()
  }
}
