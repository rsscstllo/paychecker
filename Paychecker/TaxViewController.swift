//
//  TaxViewController.swift
//  Paychecker
//
//  Created by Ross Castillo on 11/30/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//

import UIKit

class TaxViewController: UIViewController {
  
  var text: String!
  var indexPath: Int!
  var payPeriodVC: PayPeriodViewController!
  var shiftVC: ShiftViewController!
  
  @IBOutlet weak var textField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    if self.text != nil {
      textField.text = self.text
      
    } else {
      if title == "Hourly Wage" {
        textField.placeholder = "e.g. $8.50"
      }
    }
    
    textField.tintColor = UIColor(red: 33/255.0, green: 108/255.0, blue: 42/255.0, alpha: 1.0)
    textField.becomeFirstResponder()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBActions
  
  @IBAction func cancelBarButtonItemTapped(sender: UIBarButtonItem) {
    textField.resignFirstResponder()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func saveBarButtonItemTapped(sender: UIBarButtonItem) {
    if payPeriodVC != nil {
      if indexPath == 2 {
        payPeriodVC.federalIncomeTax = (textField.text! as NSString).doubleValue
      } else if indexPath == 3 {
        payPeriodVC.socialSecurityTax = (textField.text! as NSString).doubleValue
      } else if indexPath == 4 {
        payPeriodVC.medicareTax = (textField.text! as NSString).doubleValue
      }
      
    } else if shiftVC != nil {
      shiftVC.hourlyWage = (textField.text! as NSString).doubleValue
    }
    
    textField.resignFirstResponder()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
}
