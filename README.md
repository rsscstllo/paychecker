# README #

### About This Repository ###

* An iOS application intended for retail employees that calculates money earned for hourly-based paychecks –– a group-selected class project in a course I took during the fall 2015 semester.  
* v1.0.0  
* Swift 2.1, Xcode 7.2  
* Requires iOS 9.1 or later. Compatible with iPhone, iPad, and iPod touch.  

### How To Set Up ###

* Download the project, open the **Paychecker.xcodeproj** file, and run the program.  

### Source Files ###

#### Pay Periods ####
* PayPeriodsViewController.swift  
* PayPeriodTableViewCell.swift  

###### Add/Edit Pay Period ######
* PayPeriodViewController.swift  
* PayPeriodInfoTableViewCell.swift  

#### Shifts ####
* ShiftsViewController.swift  
* ShiftTableViewCell.swift  

###### Add/Edit Shift ######
* ShiftViewController.swift  
* ShiftInfoTableViewCell.swift  

#### Input ####
* PayPeriodDateViewController.swift  
* TaxViewController.swift  
* Date.swift  

#### Settings ####
* SettingsViewController.swift  
* LogOutTableViewCell.swift  
